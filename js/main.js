jQuery(document).ready(function ($) {
    $("body").smoothWheel();

    $('.modal').on('shown.bs.modal', function () {
        $(".modal").smoothWheel();
    });

    $(window).load(function () {
        //$(".preloaded").fadeIn(1000);
        $('#loader').fadeOut(500, function () {
            $(this).remove();
        });
    });

    /* main menu */
    fixedNav();
    $(window).on("scroll load", fixedNav);

    /* scrollingcarousel */
    $('.scrollingcarousel').scrollingCarousel();

    /* content */
    $('article :block').addClass('block');

    /* icon list */
    $('ul.icon-list > li').each(function () {
        var $this = $(this);
        var $icon = $('i', $this);

        if ($icon && !$('ul', $this).is('.socials')) {
            $icon.remove();
            var text = $this.html();
            text = '<span>' + text + '</span>';
            $this.html(text);
            $this.prepend($icon);
        }
    });

    /* odometer */
    var meter_span = $('.iconic-meter .meter span').toArray();
    Array.prototype.wasVisible = false;

    $(meter_span).each(function (i) {
        meter_span[i].wasVisible = false;
    });

    odometerOptions = {auto: false};

    $(window).on("scroll load", function () {
        $(meter_span).each(function (i) {
            if (elementInViewport($(this)) && meter_span[i].wasVisible === false) {
                var value = parseInt($(this).data('value'));

                var odometer = new Odometer({el: meter_span[i], value: 0, theme: 'default', format: '( ddd),dd'});
                odometer.render();
                odometer.update(value);

                meter_span[i].wasVisible = true;
            }
        });
    });


    /* parallax */
    $('[data-type="background"]').each(function () {
        var $bgobj = $(this); // создаем объект
        var objTop = $bgobj.offset().top;

        $(window).on('scroll load', function () {
            if (elementInViewport($bgobj)) {
                var yPos = -(($(window).scrollTop() - objTop) / $bgobj.data('speed')); // вычисляем коэффициент 
                // Присваиваем значение background-position
                var coords = 'center ' + yPos + 'px';
                // Создаем эффект Parallax Scrolling
                $bgobj.css({backgroundPosition: coords});

                //console.log(coords);
            }
        });
    });


    /* slider */
    $('#slider').layerSlider({
        responsive: false,
        pauseOnHover: false,
        responsiveUnder: 1140,
        layersContainer: 1140,
        skinsPath: 'css/layerslider/skins/',
        skin: 'noskin',
        navStartStop: false
    });


    /* cover image */
    $('.cover-img').each(function () {
        var $this = $(this);

        var imgSrc = $('img', this).attr('src');
        if (imgSrc) {
            $this.css('background-image', 'url("' + imgSrc + '")');
            if (!$this.hasClass('thumb-wrap'))
                $('img', this).remove();
        }
    });


    /* portfolio items */
    var $grid = $('.grid').isotope({
        // options
        itemSelector: '.grid-item',
        layoutMode: 'masonry'
    });

    $('.portfolio-filter a').click(function (e) {
        e.preventDefault();

        var filterValue = $(this).attr('data-filter');
        $grid.isotope({filter: filterValue});

        $('.portfolio-filter a').removeClass('active');
        $(this).addClass('active');
    });


    /* our team */
    $('.slicks').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 4,
        slidesToScroll: 1,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.our-clients').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });


    $('.carousel').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        swipeToSlide: true
    });

    $('.slicks-rounded .slick-arrow').addClass('rounded');
    $('.slicks-rounded-big .slick-arrow').addClass('rounded-big');


    $('.group-popup-container').magnificPopup({
        delegate: '.popup-link:visible',
        type: 'image',
        mainClass: 'mfp-fade',
        gallery: {
            enabled: true, // set to true to enable gallery

            preload: [0, 2], // read about this option in next Lazy-loading section

            navigateByImgClick: true,
            arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button

            tPrev: 'Previous (Left arrow key)', // title for left button
            tNext: 'Next (Right arrow key)', // title for right button
            tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
        },
        callbacks: {
            open: function () {
                $('.mfp-container, .mfp-close').awesomeCursor('search-minus', {
                    color: 'rgba(255, 255, 255, 1)',
                    size: 20
                });
            }
        }
    });

    $('.popup-single-link').magnificPopup({
        type: 'image',
        mainClass: 'mfp-fade',
        callbacks: {
            open: function () {
                $('.mfp-container, .mfp-close').awesomeCursor('search-minus', {
                    color: 'rgba(255, 255, 255, 1)',
                    size: 20
                });
            }
        }
    });


    window.sr = new scrollReveal();


    /* contact form AJAX */
    // Variable to hold request
    var request;

    // Bind to the submit event of our form
    $("#contact-form").submit(function (event) {

        // Abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $(this);

        // Let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");

        // Serialize the data in the form
        var serializedData = $form.serialize();

        // Let's disable the inputs for the duration of the Ajax request.
        // Note: we disable elements AFTER the form data has been serialized.
        // Disabled form elements will not be serialized.
        $inputs.prop("disabled", true);

        // Fire off the request to /form.php
        request = $.ajax({
            url: $form.attr('action'),
            type: "post",
            data: serializedData
        });

        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            $('.alert-success').fadeIn(300);
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            console.log(
                    "The following error occurred: " +
                    textStatus, errorThrown
                    );

            $('.alert-danger').fadeIn(300);
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // Reenable the inputs
            $inputs.prop("disabled", false);
        });

        // Prevent default posting of form
        event.preventDefault();
    });


    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    
    dropDownMenu();
    $(window).on('resize', dropDownMenu);

    var $dropdown = $(".dropdown");

    $dropdown.each(function () {
        var $this = $(this);

        var $dropmenu = $this.find(".dropdown-menu");
        $dropmenu.css("height", $dropmenu.outerHeight());
        $this.addClass("drop-collapsed");
    });

});

function showMenu() {
    $(this).removeClass("drop-collapsed");
    $(this).addClass("open");
}
function hideMenu() {
    $(this).removeClass("open");
    var $dropdown = $(".dropdown");

    $dropdown.each(function () {
        $(this).addClass("drop-collapsed");
    });
}
function dropDownMenu() {

    $(".dropdown").unbind("mouseenter").unbind("mouseleave");
    $(".dropdown").removeProp('hoverIntent_t');
    $(".dropdown").removeProp('hoverIntent_s');

    if ($(window).width() > 767) {

        // dropdown menu hover intent
        var hovsettings = {
            timeout: 0,
            interval: 0,
            over: showMenu,
            out: hideMenu
        };

        $(".dropdown").hoverIntent(hovsettings);
    }
}


function fixedNav() {
    if ($(window).width() > 767) {
        var $header_nav = $("body");
        var cls = "header-fixed";
        if ($(window).scrollTop() > 150) {
            $header_nav.addClass(cls);
        }
        else {
            $header_nav.removeClass(cls);
        }

        $header_nav = $("#header-nav");
        cls = "shown";
        if ($(window).scrollTop() > 450) {
            $header_nav.addClass(cls);
        }
        else {
            $header_nav.removeClass(cls);
        }
    }
}

function elementInViewport(el) {
    var top = el.offset().top;
    var bottom = el.offset().top + el.outerHeight();
    var viewPort = $(window).scrollTop() + $(window).innerHeight();

    return viewPort > top && $(window).scrollTop() < bottom;
}


$.extend($.expr[':'], {
    block: function (a) {
        var tagNames = {
            "ADDRESS": true, "BLOCKQUOTE": true, "CENTER": true, "DIR": true, "DIV": true,
            "DL": true, "FIELDSET": true, "FORM": true, "H1": true, "H2": true, "H3": true,
            "H4": true, "H5": true, "H6": true, "HR": true, "ISINDEX": true, "MENU": true,
            "NOFRAMES": true, "NOSCRIPT": true, "OL": true, "P": true, "PRE": true, "TABLE": true,
            "UL": true, "DD": true, "DT": true, "FRAMESET": true, "LI": true, "TBODY": true,
            "TD": true, "TFOOT": true, "TH": true, "THEAD": true, "TR": true
        };
        return $(a).is(function () {
            if (tagNames[this.tagName.toUpperCase()]) {
                if (this.style.display === "block")
                {
                    return true;
                }
                if (this.style.display !== "" || this.style.float !== "")
                {
                    return false;
                }
                else {
                    return $(this).css("display") === "block";
                }
            }
            else {
                if (this.style.display === "block") {
                    return;
                }
                else {
                    return $(this).css("display") === "block";
                }
            }
        });
    }
});