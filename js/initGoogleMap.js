/* google map */
function initialize() {
    var myLatlng = new google.maps.LatLng(-34.397, 150.644);
    var mapOptions = {
        zoom: 8,
        center: myLatlng,
        scrollwheel: false
    };
    var map = new google.maps.Map(document.getElementById('google-map'),
            mapOptions);

    var marker = new google.maps.Marker({
        icon: 'img/marker.png',
        position: myLatlng,
        map: map,
        title: 'Mirror Template'
    });

    var content = document.createElement('div');
    content.innerHTML = "<h4>Mirror Template</h4>PO Box 16134<br />Victoria Street<br />West Victoria 513<br />New Yourk, Us";
    var infowindow = new google.maps.InfoWindow({
        content: content
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
}

google.maps.event.addDomListener(window, 'load', initialize);